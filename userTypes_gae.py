#!/usr/bin/python
# -*- coding: utf-8; mode: python -*-


from google.appengine.ext import ndb

class Album(ndb.Model):
    #title = ndb.StringProperty()    
    artist = ndb.StringProperty()
    year = ndb.DateProperty()
    
    def album2json(self):
        return {"artist":self.artist}

    
class Songs(ndb.Model):
    title = ndb.StringProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)

    def song2json(self):
        return {"title":self.title}

    @classmethod
    def toJSONlist(self, entriesList):
        auxJSON = []
        for itSongs in entriesList:
            auxJSON.append(itSongs.song2json())
        return auxJSON
    
    @classmethod
    def getAll(self):
        return self.toJSONlist(Songs.query())


    '''
class Playlists(ndb.Model):
'''
    
