#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from datetime import date, datetime
from google.appengine.ext import ndb
from google.appengine.api import memcache
from flask import Flask, jsonify, abort, make_response, request, url_for

from userTypes_gae import Songs, Album

app = Flask(__name__)
app.config['DEBUG'] = True



@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not Found'}), 404)


# Queries
@app.route('/search')
def search():
    title = request.args.get('title',"", type=str)
    auxJSON = Songs.toJSONlist(
        Songs.query(Songs.title==title))
    return make_response(jsonify({"songs":auxJSON}), 200)


@app.route('/searchGQL')
def searchGQL():
    title = request.args.get('title',"", type=str)
    auxJSON = Songs.toJSONlist(
        ndb.gql("SELECT * FROM Songs WHERE title='"+
                title +"'"))
    return make_response(jsonify({"songs":auxJSON}), 200)


# OPERACIONES sobre Albums
@app.route('/albums', methods = ['POST'])
def addAlbum():
    if not request.json or not 'title' in request.json:
        abort(400)
    title = request.json['title']
    strYear = request.json.get('year',"1900-01-01")
    year = datetime.strptime(strYear,"%Y-%m-%d")
    newAlbum = Album(
        id=title,
        artist=request.json.get('artist',"anonymous"),
        year=year)
        
    idAlbum=newAlbum.put()
    memcache.flush_all()
    return make_response (jsonify({"created":idAlbum.urlsafe()}), 201)
    


# OPERACIONES sobre songs
@app.route('/songs/<id_song>', methods = ['GET'])
def get_song(id_song):
    title = memcache.get(id_song)
    if title is None:
        try:
            key = ndb.Key(urlsafe=id_song)
        except:
            abort(404)
        title = (key.get()).title
        memcache.add(id_song, title, 120)
    return make_response(jsonify({"song":title}), 200)


@app.route('/songs/<id_song>', methods = ['DELETE'])
def del_song(id_song):
    try:
        key = ndb.Key(urlsafe=id_song)
    except:
        abort(404)
    key.delete()
    memcache.delete(id_song)
    return make_response(jsonify({"deleted":id_song}), 200)


def getSongs(albumID):
    auxJSON = []
    if albumID == "":
        auxJSON=Songs.getAll()
    else:
        key = ndb.Key('Album', albumID)
        songList = Songs.query(ancestor=key)
        auxJSON = Songs.toJSONlist(songList)
    return make_response(jsonify({"songs":auxJSON}), 200)


def addSong(): 
    if not request.json or not 'title' in request.json or not 'albumID' in request.json:
        abort(400)
    title = request.json['title']
    albumID = request.json['albumID']
    newSong = Songs(
        parent=ndb.Key('Album', albumID),
        title=title)
    idSong=newSong.put()
    memcache.add(idSong.urlsafe(), newSong.title, 300)
    return make_response (jsonify({"created":idSong.urlsafe()}), 201)


@app.route('/songs', methods = ['GET', 'POST'])
def manager_songs():
    if request.method == 'POST':
        return addSong()
    elif request.method == 'GET':
        albumID = request.args.get('album',"", type=str)
        return getSongs(albumID)




    
# OPERACIONES sobre playlists

@app.route('/playlists', methods = ['GET'])
def get_playlists(): pass


def delPlayList(id_ps): pass

def getPlayList(id_ps): pass

def addPlayList(id_ps): pass
    

@app.route('/playlists/<path:id_ps>', methods = ['DELETE', 'PUT', 'GET'])
def manager_playlist(id_ps):
    if request.method == 'GET':
        return getPlayList(id_ps)
    elif request.method == 'PUT':
        return addPlayList(id_ps)
    elif request.method == 'DELETE':
        return delPlayList(id_ps)


@app.route('/playlists/<id_ps>/songs', methods = ['POST'])
def addSongToAPlayList(id_ps): pass


@app.route('/playlists/<id_ps>/songs/<id_song>', methods = ['DELETE'])
def delSongOfAPlayList(id_ps, id_song): pass
